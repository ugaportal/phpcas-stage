#!/usr/bin/php
<?php
header('Content-Type:text/plain charset=utf-8');
/**
* Get input from command line.
*
* @param string prompt for user to read.
* @return string value typed on the command line
*/
#
# Util class for color coded command line output
require_once(__DIR__ . "/src/util.php");
$c = new Util;

printf("%sGET root and intermediate certs%s. \n", $c->header, $c->endc);
$host = "";

#
# Attempt to get host from command line arguments.  If none present ask the user for it.
if(count($argv) > 1 && (substr($argv[1], 0, 3) === "cas" || substr($argv[1], 0, 3) === "sso"))
	$host = $argv[1];
else {
	do { 
		$host = $c->scanf(
			sprintf("%sEnter hostname (e.g. cas2.uga.edu)%s: ", $c->blue, $c->endc)
		); 
	} while( empty( $host ) );
}

#
# Get the cert from the server and save it as servername.pem (e.g. cas.uga.edu.pem)
$output = "";
$destfile = __DIR__."/".$host.".pem";
try {
	$ctx = stream_context_create(["ssl" => [ "verify_peer" => 0, "capture_peer_cert_chain" => 1, "SNI_enabled" => 1 ]]);
	$read = stream_socket_client("ssl://" . $host . ":443", $errno, $errstr, 3, STREAM_CLIENT_CONNECT, $ctx);
	$cert = stream_context_get_params($read);

	foreach($cert['options']['ssl']['peer_certificate_chain'] as $chain){
		openssl_x509_export($chain, $certchain);
		$output .= $certchain;
	}
} catch (Exception $e) { 
	echo $e->getMessage(); 
}
file_put_contents($destfile, $output);

#
# Print message and exit
printf($output);
printf("\n");
printf("%sCert %s saved to %s%s\n",$c->green, basename($destfile), dirname($destfile), $c->endc);
?>