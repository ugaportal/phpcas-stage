<?php
#error_reporting(E_ERROR);
#ini_set('display_errors', '1');
#
# Root Directory
define("CAS_ROOT", dirname(__FILE__)); // Set CAS_ROOT 
#
# CAS variables
$config = json_decode(file_get_contents(CAS_ROOT."/sso_config.json"));
define("CAS_SERVER",  $config->cas_server);
define("CAS_DOMAIN",  $config->cas_domain);
define("CAS_CONTEXT", $config->cas_context);
define("CAS_PORT", 	  $config->cas_port);
define("CAS_VERSION", $config->cas_version);
#
# CAS file location variables
define("CAS_LOG", CAS_ROOT."/log"); // Log file for CAS trace reports
$phpcas_path = CAS_VERSION === "v1.4.0" 
	? CAS_ROOT."/vendor/apereo/phpcas" // Use the Composer version
	: CAS_ROOT.DIRECTORY_SEPARATOR.CAS_VERSION; // Use the github version
#
# Location of PHP CAS source code and the CAS.php file
define("CAS_SOURCE", $phpcas_path);
#
# PEM files for Simple Configuration for CAS.
# Production key file from the CAS server
define("PRODCAS_KEY", implode(".", [CAS_ROOT.DIRECTORY_SEPARATOR.CAS_SERVER, CAS_DOMAIN, "pem"])); 
#
# Initial setup to avoid displaying certain PHP errors.
date_default_timezone_set("America/New_York");
libxml_use_internal_errors(true);

function cas_init($mode="prod", $saml=false)
{	
	#
	# Setup CAS for the initilization
	#
	# Can be set up from the command line using setup.php
	# e.g. _$ ./setup.php sso.dev.uga.edu
	$server = CAS_SERVER . ($mode === "dev" || $mode === "stage" ? ".$mode." : ".") . CAS_DOMAIN;
	#
	# Set the session-name to be unique to the current script so that 
	# the client script doesn't share its session with a proxied script.
	# This is just useful when running the example code, but not normally.
	session_name(
		'session_for-'
		. preg_replace('/[^a-z0-9-]/i', '_', basename($_SERVER['SCRIPT_NAME']))
	);
	#
	# Set header
	header('Content-Type: text/html; charset=utf-8');
	
	#
	# Include the CAS libary and initilize the static phpCAS Class.
	require_once(CAS_SOURCE.DIRECTORY_SEPARATOR."CAS.php");
	
	#
	# Enable debugging
	phpCAS::setLogger();
	#
	# Enable verbose error messages. Disable in production!
	if($mode !== "prod")
		phpCAS::setVerbose(true);
	#
	# Set to protocol CAS SAML or CAS WITHOUT SAML
	if($saml)
		phpCAS::client(SAML_VERSION_1_1, $server, CAS_PORT, CAS_CONTEXT);
	else
		phpCAS::client(CAS_VERSION_2_0, $server, CAS_PORT, CAS_CONTEXT);
	
	#
	# Salt for further SessionID protection in cases where someone got 
	# the ticket from the URL who wasn't supposed to.
	if(isset($config->cas_salt) && !empty($config->cas_salt))
		phpCAS::setSessionIdSalt($salt);
	
	#
	# For production use set the CA certificate that is the issuer of the cert
	# on the CAS server and uncomment the line below
	if($mode === "prod")
		phpCAS::setCasServerCACert(PRODCAS_KEY);
	
	
	# Handle SAML logout requests that emanate from the CAS host exclusively.
	# Failure to restrict SAML logout requests to authorized hosts could
	# allow denial of service attacks where at the least the server is
	# tied up parsing bogus XML messages.
	phpCAS::handleLogoutRequests(true, [
		CAS_SERVER.CAS_DOMAIN
	]);

	# For quick testing you can disable SSL validation of the CAS server.
	# THIS SETTING IS NOT RECOMMENDED FOR PRODUCTION.
	# VALIDATING THE CAS SERVER IS CRUCIAL TO THE SECURITY OF THE CAS PROTOCOL!
	if($mode !== "prod") 
		phpCAS::setNoCasServerValidation();

	if (isset($_GET['logout']))
		phpCAS::logout();
}
#
# Harden session cookie to prevent some attacks on the cookie (e.g. XSS)
function hardening() {
	#
	# Client config for cookie hardening
	$client_domain = $_SERVER["HTTP_HOST"];
	$client_path = dirname($_SERVER["PHP_SELF"]);
	$client_secure = true;
	$client_httpOnly = true;
	$client_lifetime = 0;
	#
	# Harden session cookie to prevent some attacks on the cookie (e.g. XSS)
	session_set_cookie_params($client_lifetime, $client_path, $client_domain, $client_secure, $client_httpOnly);

}
#
# Stuff for PGT tickets, not being used.
function pgt_init(){
	#
	# New configuration for version 1.4.0	
	#
	#
	# Database config for PGT Storage
	$db = 'pgsql:host=localhost;dbname=phpcas';
	//$db = 'mysql:host=localhost;dbname=phpcas';
	$db_user = 'phpcasuser';
	$db_password = 'mysupersecretpass';
	$db_table = 'phpcastabel';
	$driver_options = '';

	# Generating the URLS for the local cas example services for proxy testing
	$curbase = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on'
		? 'https://' . $_SERVER['SERVER_NAME']
		: 'http://' . $_SERVER['SERVER_NAME'];
	
	$curbase .= $_SERVER['SERVER_PORT'] != 80 && $_SERVER['SERVER_PORT'] != 443 
		? ':' . $_SERVER['SERVER_PORT']
		: '';

	$curdir = dirname($_SERVER['REQUEST_URI']) . "/";

	#
	# CAS client nodes for rebroadcasting pgtIou/pgtId and logoutRequest
	$rebroadcast_node_1 = 'http://cas-client-1.example.com';
	$rebroadcast_node_2 = 'http://cas-client-2.example.com';

	// access to a single service
	$serviceUrl = $curbase . $curdir . 'example_service.php';
	// access to a second service
	$serviceUrl2 = $curbase . $curdir . 'example_service_that_proxies.php';

	$pgtBase = preg_quote(preg_replace('/^http:/', 'https:', $curbase . $curdir), '/');
	$pgtUrlRegexp = '/^' . $pgtBase . '.*$/';

	$cas_url = 'https://' . $cas_host;
	if ($cas_port != '443') {
		$cas_url = $cas_url . ':' . $cas_port;
	}
	$cas_url = $cas_url . $cas_context;
}
?>
