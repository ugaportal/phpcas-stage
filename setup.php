#!/usr/bin/php
<?php
header('Content-Type:text/plain charset=utf-8');
/**
* Install a version of CAS on the server and then download the public key.
*/
#
# Util class for color coded command line output
require_once(__DIR__ . "/src/util.php");
$c = new Util;
#
# Clear the screen
$cls = system("clear");
#
# Print opening message.
printf("%s#####################################################################
# This shell script will set up sso_app.php and/or cas_app.php which
# run in the public html folder.
# 
# Type the url that you are setting up (e.g. sso.uga.edu).
# This URL will be used as the endpoint in the selected CAS app.
#
#####################################################################\n%s", $c->header, $c->endc);
define("CAS_ROOT", dirname(__FILE__));
define("CAS_CONFIG", dirname(__FILE__)."/cas_config.json");
define("SSO_CONFIG", dirname(__FILE__)."/sso_config.json");
define("DOWNLOAD_KEY_SCRIPT", dirname(__FILE__)."/get-cert.php");
define("DEFAULT_HOST", "sso.uga.edu");
define("DEFAULT_VERSION", "v1.4.0");
#
# About this program:
printf("%s#\n# Initailizing CAS for use and download the proper certs.%s \n", $c->header, $c->endc);
#
# Attempt to get host from command line arguments.  If none present ask the user for it.
$host = "";
do { 
	$host = $c->scanf(
		sprintf("%sEnter hostname (default: %s). Press enter for default %s: ", $c->blue, DEFAULT_HOST, $c->endc)
	); 
	if(empty($host))
		$host = DEFAULT_HOST;
} while( empty( $host ) );
#
# Get the version. Default to folder in current directory with highest version.
$versions = [];
foreach(scandir(dirname(__FILE__)) as $key => $filename)
	if(substr($filename, 0, 1) === "v")
		$versions[] = $filename;

$selected = trim($c->scanf(
	sprintf("%sEnter version which will run (default is: %s). Press ENTER to use the default version.%s: ", $c->blue, DEFAULT_VERSION, $c->endc)
));
$selected_version = empty( $selected ) ? DEFAULT_VERSION : $selected;

#
# Attempt to get a salt for making PHP SESSION more secure after log in.
$salt = trim($c->scanf(
	sprintf("%sEnter a salt to be used for security. Leave blank for no salt. %s: ", $c->blue, $c->endc)
)); 
#
# remove any newline chars and 
# chars that might cause errors from salt.
if($salt){
	$salt = preg_replace("/\W/", "", $salt);
	printf("%sWe will add your salt:%s %s%s%s\n", $c->blue, $c->endc, $c->bold, $salt, $c->endc);
}
#
# separate $host into parts e.g $host = "sso.dev.uga.edu"
# cas_domain will equal "uga.edu"
# cas_server will equal "sso.dev"
$parts = explode(".", $host);
$domain_arr = array_slice($parts, -2); // Get last two sections of cas url
$config = [];
$config["cas_domain"] = trim(implode(".", $domain_arr), "."); // Should be uga.edu
$config["cas_server"] = trim(implode(".", array_diff($parts, $domain_arr)), "."); // should be cas or sso variant.
#
# setup 
if(!file_exists(SSO_CONFIG)) {
	$config["cas_context"] = "/cas";
	$config["cas_port"] = 443;
	$config["cas_salt"] = $salt;
	$config["cas_version"] = $selected_version;
	file_put_contents(SSO_CONFIG, json_encode($config, JSON_UNESCAPED_SLASHES));
} else {
	$old = json_decode(file_get_contents(SSO_CONFIG));
	file_put_contents(SSO_CONFIG, json_encode([
		"cas_server" => $config["cas_server"],
		"cas_domain" => $config["cas_domain"],
		"cas_context" => $old->cas_context,
		"cas_port" => $old->cas_port,
		"cas_salt" => $salt,
		"cas_version" => $selected_version
	], JSON_UNESCAPED_SLASHES));
}
#
# Attempt to get a salt for making PHP SESSION more secure after log in.
$composer = '';
do {
	$composer = trim($c->scanf(
		sprintf("%sDo you want to install the CAS libraries from\nhttps://packagist.org/packages/apereo/phpcas? [y/n] %s: ", $c->blue, $c->endc)
	)); 
} while( empty( $composer ) );

if($composer === 'y' || $composer === 'Y')
	shell_exec(__DIR__ . "/composer-install.php");

#
# Download the cert by running get-cert.php
# if get-cert.php doesn't exist, or the cert file doesn't exist, print an error message.
$certfile = CAS_ROOT."/".$host.".pem";
if(!file_exists(DOWNLOAD_KEY_SCRIPT) && !file_exists($certfile) )
	printf("%s# %s does not exist and download script not found.  Cert will have to be downloaded manually from%s.%s\n", $c->warning, $certfile, $host, $c->endc);
else
	printf("#\n# %s\n#\n", exec(DOWNLOAD_KEY_SCRIPT . " " . $host));
#
# Successful Exit	
exit(sprintf("# %sSetup Complete.%s\n", $c->bold, $c->endc));
?>