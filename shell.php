#!/usr/bin/php
<?php
#
# Directory that will be have user's permissions set to use CAS/SSO
define("CWD", __DIR__);
#
# Set what log file will be created
define("LOGFILE", __DIR__.DIRECTORY_SEPARATOR."log");
#
# Group authorized to use SSO/CAS Library
define("AUTH_GROUP", "phpcasgrp");
#
# File read to determine the users which will have group membership added.
define("GROUP_FILE", "/etc/domains");

#
# Util class for color coded command line output
require_once(__DIR__ . "/src/util.php");
$c = new Util;
#
# Main Running part
#
# Clear the screen
$cls = system("clear");

#
# Print opening message.
printf("%s################################################################
# This shell script will set up phpCAS for multiple domains/accounts
# It was written with the lexiconn environment in mind, so it may not work 
# correctly if you use some other hosting.
#
# First, it will create a group called AUTH_GROUP 
# Then it will add all the current domains' users (listed in $domains) to it.
# Then it will chmod the CWD directory, making AUTH_GROUP the owner.
# Finally, it will create a log file and have it world writable ( must be 777 )
#
# You must run this script with \"sudo ./shell.php\"
# because it requires root privileges.
###############################################################\n%s", $c->header, $c->endc);
#
# Find out if user wants to proceed.
$answer = "";
do { 
	$answer = $c->scanf(
		sprintf("%sDo you wish to proceed?\nType 'yes' to proceed %s: ", $c->blue, $c->endc)
	); 
} while( !$answer === "yes" || !$answer );
#
# Run in Dev mode where we only change the groups for the current user
$mode = "";
printf("%sRunning in prod mode will modify add %s to each user in %s%s\n", $c->blue, AUTH_GROUP, GROUPFILE, $c->endc);
while( !($mode === "dev" || $mode === "prod") ){
	$mode = $c->scanf(
		sprintf("%sEnter 'dev' or 'prod'%s: ", $c->blue, $c->endc)
	); 
}

#
# Proceed if they want to!
if($answer === "yes"){
	
	#
	# CHMOD the CWD directory to 750 (owner/group access)
	$command = shell_exec("chmod -R 750 " . CWD);
	if( $command )
		printf("%s# %s%s", $c->blue, $command, $c->endc);
	#
	# create the group AUTH_GROUP to access phpcas
	$command = shell_exec("groupadd -f " . AUTH_GROUP);
	if( $command )
		printf("%s# %s%s", $c->blue, $command, $c->endc);
	#
	# Add all accounts in $domains to the AUTH_GROUP
	# one liner for shell: sed 's/grp//g' $domains | awk '{print $3}' | gpasswd -a AUTH_GROUP
	
	$users = $mode === "prod" ? $c->extract_users( GROUP_FILE ) : [ trim(shell_exec("whoami")) ];

	$c->add_users_to_group($users, AUTH_GROUP);
	#
	# Make AUTH_GROUP the owner of the CWD directory
	$whoami = explode(" ", trim(shell_exec("users")))[0];
	
	#$command = shell_exec("chown -R $whoami:" . AUTH_GROUP . " " . CWD);
	echo "whoami: ", "'".$whoami."'\n";
	echo "group: ", "'".AUTH_GROUP."'\n";

	$command = shell_exec(sprintf("chown -R %s:%s %s", $whoami, AUTH_GROUP , CWD));
	if( $command )
		printf("%s# %s%s", $c->green, $command, $c->endc);
	#
	# Create and edit permissions for CAS log
	touch(LOGFILE);
	$command = shell_exec("chmod 777 " . LOGFILE);
	if( $command )
		printf("%s# %s%s", $c->green, $command, $c->endc);
}
printf("%s#\n# Program ended%s\n", $c->blue, $c->endc);
exit(0);
