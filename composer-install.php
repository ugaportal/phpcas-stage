#!/usr/bin/php
<?php
header('Content-Type:text/plain charset=utf-8');
define("HOME", $_SERVER["HOME"]);
/**
* Get input from command line.
*
* @param string prompt for user to read.
* @return string value typed on the command line
*/
#
# Util class for color coded command line output
require_once(__DIR__ . "/src/util.php");
$c = new Util;

printf("%s#\n# Download/install Composer%s. \n", $c->header, $c->endc);
#
# INSTALL COMPOSER
# Steps to install composer from https://getcomposer.org/download/
# This is not a global install, but only local.  To perform a global install,
# To do a global install, run this command after this script has finished:
# `sudo mv composer.phar /usr/local/bin/composer`
unlink('composer.phar');
copy('https://getcomposer.org/installer', 'composer-setup.php');
if (hash_file('sha384', 'composer-setup.php') === '906a84df04cea2aa72f40b5f787e49f22d4c2f19492ac310e8cba5b96ac8b64115ac402c8cd292b8a03482574915d1a8') { 
	printf("Installer verified\n"); 
} else { 
	printf('Installer corrupt\n'); 
	unlink('composer-setup.php'); 
}
shell_exec('php composer-setup.php --install-dir=' . __DIR__);
unlink('composer-setup.php');
#
# INSTALL NEWEST VERSION OF CAS.
printf("%s#\n# Install Jasig PHPCas%s. \n", $c->header, $c->endc);
echo shell_exec('composer require apereo/phpcas');
?>