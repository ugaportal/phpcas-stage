<?php
#
# Util class for color coded command line output
class Util {
	var $header = "\033[95m";
    var $blue = "\033[94m";
    var $green = "\033[32m";
    var $warning = "\033[33m";
    var $fail = "\033[91m";
    var $endc = "\033[0m";
    var $bold = "\033[1m";
    var $underline = "\033[4m";
    
    public function scanf($msg)
	{
		printf($msg);
		$handle = fopen ("php://stdin","r");
		$line = fgets($handle);
		fclose($handle);
	
		return trim($line);
	}
	
	private function create_group($group)
	{
		$result = shell_exec("groupadd $group");
		printf("%s\n", $result);
	}
	
	public function group_exists($group)
	{
		return strpos( trim(shell_exec("groups")), $group) === true;
	}
	#
	# Add all accounts in $domains to the AUTH_GROUP
	# one liner for shell: sed 's/grp//g' $domains | awk '{print $3}' | gpasswd -a AUTH_GROUP	
	public function extract_users($file)
	{
	#
	#
		if(!file_exists( $file ))
			exit(
				sprintf("%s#\n# Something went wrong.  Check to see whether '%s' exists or not.%s\n", 
					$c->fail, 
					$domains, 
					$c->endc
				)
			);
	
		$users = [];
		foreach( file( $file ) as $line ){
				list( $arecord, $ip, $group, $acct ) = explode(" ", $line);
				$users[] = str_replace("grp", "", $group);
		}
		return $users;
	}
	
	private function add_group_membership($user, $group){
		if(! $user || ! $group)
			return false;
			
		$command = shell_exec("gpasswd -a $user $group");
		printf("%s# %s%s", $c->green, $command, $c->endc);
		return true;
	}
	
	public function add_users_to_group($users, $group)
	{
		if(!$this->group_exists($group))
			$this->create_group($group);
			
		foreach( $users as $user )
			$this->add_group_membership($user, $group);
	}
}
