phpCAS
=======

phpCAS is an authentication library that allows PHP applications to easily authenticate
users via a Central Authentication Service (CAS) server.

Please see the wiki website for more information:

https://wiki.jasig.org/display/CASC/phpCAS

Api documentation can be found here:

https://apereo.github.io/phpCAS/


[![Build Status](https://travis-ci.org/apereo/phpCAS.png)](https://travis-ci.org/apereo/phpCAS)


LICENSE
-------

Copyright 2007-2015, JA-SIG, Inc.
This project includes software developed by Jasig.
http://www.jasig.org/

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this software except in compliance with the License.
You may obtain a copy of the License at:

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

EASY SETUP
----------
1. Clone the phpCAS repo to your website's home directory (e.g. /home/site-name). 
2. From the command line run ./phpcas/setup.php.  Follow **Setup Steps**
3. Enter the version of CAS you will be running.  Just press enter for the latest version.
4. If the CAS cert does not exist, get-cert.php will attempt to download it.
5. Reference the CAS code in your script:

SETUP STEPS
-----------
1. Run ./setup.php within the phpcas directory
```
#####################################################################
# This shell script will set up sso_app.php and/or cas_app.php which
# run in the public html folder.
# 
# Type the url that you are setting up (e.g. sso.uga.edu).
# This URL will be used as the endpoint in the selected CAS app.
#
#####################################################################
#
# Initailizing CAS for use and download the proper certs. 
Enter hostname (default: sso.uga.edu). Press enter for default : 
Enter version which will run (default is: v1.4.0). Press ENTER to use the default version.: v1.3.8
Enter a salt to be used for security. Leave blank for no salt. : samplerandomsalt
We will add your salt: samplerandomsalt
# Cert sso.uga.edu.pem saved to /home/sitename/phpcas
#
```

USING THE LIBRARY
-----------------
### Dev or Staging
```
<?php
require_once(dirname($_SERVER["DOCUMENT_ROOT"])."/phpcas/sso_app.php"); 
cas_init("stage"); // "dev" for development
phpCAS::forceAuthentication();
$user = phpCAS::getUser();
?>
```
### Production
```
<?php
require_once(dirname($_SERVER["DOCUMENT_ROOT"])."/phpcas/sso_app.php"); 
cas_init(); 
phpCAS::forceAuthentication();
$user = phpCAS::getUser();
?>
```
### Production with SAML attributes
```
<?php
require_once(dirname($_SERVER["DOCUMENT_ROOT"])."/phpcas/sso_app.php"); 
cas_init("prod", true); 
phpCAS::forceAuthentication();
$user = phpCAS::getUser();
$attributes = phpCAS::getAttributes();
?>
```

MULTIPLE ACCOUNTS ON VPS INSTALL
--------------------------------
sudo privileges are required to do a multiple account install.

1. Log into to root account and cd to the /opt directory
2. Clone the repo and cd into the /opt/phpcas directory
3. Type ```sudo ./shell.php``` to allow all the VPS accounts access
4. Type ```./setup.php``` to install phpCAS for a particular instance of CAS as well as download the proper public key file.
5. In your VPS accounts that will be using the global phpCAS, you will need to modify the PHP code slightly:

IMPORTANT!
----------
**When running for multiple accounts under one VPS, it is VERY important to run ```sudo ./setup.php``` IMMEDIATELY after a ```git pull```.**

### Staging
```
<?php
require_once("/opt/phpcas/sso_app.php"); 
cas_init("stage"); 
phpCAS::forceAuthentication();
$user = phpCAS::getUser();
?>
```
### Production
```
<?php
require_once("/opt/phpcas/sso_app.php"); 
cas_init(); 
phpCAS::forceAuthentication();
$user = phpCAS::getUser();
?>
```
### Production with SAML attributes
```
<?php
require_once("/opt/phpcas/sso_app.php"); 
cas_init("prod", true); 
phpCAS::forceAuthentication();
$user = phpCAS::getUser();
$attributes = phpCAS::getAttributes();
?>
```

FIXING ISSUES
-------------
If you get error messages telling people that they need to log in again even though they have already logged in,
it may be due to an invalid cert.  To get the correct cert from CAS, run **get-cert.php**.